package com.example.pomotimetracker

import Utility.Data
import Utility.Project
import Utility.WorkSessionEvent
import android.app.Application
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.Toast
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.shashank.sony.fancytoastlib.FancyToast
import org.apache.commons.io.FileUtils
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.io.File
import java.io.IOException

const val TAG = "PomoTimer"

class ApplicationPOMO : Application() {
    companion object {
        lateinit var data : Data
        lateinit var context : Context
    }

    override fun onCreate() {
        super.onCreate()
        context = this
        data = Data()

        gson = GsonBuilder().setPrettyPrinting().create()
        file = File(filesDir, DATA_FILE_NAME)

        load()

        //register to session event
        EventBus.getDefault().register(this)
    }

    private val DATA_FILE_NAME = "pomodata.txt"
    private lateinit var gson : Gson
    private lateinit var file : File

    private fun saveToFile(){
        try{
            FileUtils.writeStringToFile(file, gson.toJson(data))
        }catch (e : IOException){
            Log.e(TAG, "Ni mozno shraniti v: " + file.path)
        }
    }
    private fun readFromFile() : Boolean{
        if(!file.exists())
            return false
        try{
            data = gson.fromJson(FileUtils.readFileToString(file), Data::class.java)
        }catch (e : IOException){
            return false
        }
        return true
    }

    fun save(){
        saveToFile()
    }
    fun load(){
        readFromFile()
    }

    @Subscribe
    fun onEvent(event: WorkSessionEvent){
        if(event.getSessionResult())
        {
            data.projects[event.getProjectId()].sessionFinish()
            FancyToast.makeText(this, "Session Finished!", FancyToast.LENGTH_LONG, FancyToast.SUCCESS, false).show()
        }
    }
}
