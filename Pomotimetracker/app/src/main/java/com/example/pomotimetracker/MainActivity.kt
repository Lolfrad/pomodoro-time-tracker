package com.example.pomotimetracker

import Utility.Alert
import Utility.RadialProgressBar
import Utility.Timer
import Utility.WorkSessionEvent
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Vibrator
import android.support.v4.content.ContextCompat
import android.widget.Button
import android.widget.TextView
import com.shashank.sony.fancytoastlib.FancyToast
import com.varunest.sparkbutton.SparkButton
import org.greenrobot.eventbus.EventBus


class MainActivity : AppCompatActivity() {
    private lateinit var titleText : TextView
    lateinit var timerText : TextView
    lateinit var progressBar : RadialProgressBar
    lateinit var sparkButton : SparkButton

    val sessionLength : Long = 60 * 1 * 1000 / 6 // 10 s

    var sessionStarted = false
        private set(value) {field = value}

    lateinit var vibratorService : Vibrator

    private lateinit var  timer : Timer
    private lateinit var alert : Alert

    private var projectId = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        progressBar = findViewById(R.id.custom_progressBar)
        timerText = findViewById(R.id.timer_textView)
        titleText = findViewById(R.id.textView_timerTitle)
        sparkButton = findViewById(R.id.workSession_sparkButton)

        sparkButton.setOnClickListener{
            if(sessionStarted)
            {
                sparkButton.isChecked = false
                timer.PauseSession()
                sessionStarted = false
                FancyToast.makeText(this, "Session Cancelled", FancyToast.LENGTH_SHORT, FancyToast.ERROR, false).show()
            }
            else
            {
                sparkButton.playAnimation()
                sparkButton.isChecked = true
                timer.StartSession()
                sessionStarted = true
            }
        }

        vibratorService = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

        timer = Timer(this)
        alert = Alert(this)

        //set title text
        val pos = intent.getStringExtra("Pos").toInt()
        projectId = pos
        titleText.text = ApplicationPOMO.data.projects[pos].name
        //set colors
        val color = ContextCompat.getColor(ApplicationPOMO.context, ApplicationPOMO.data.projects[pos].colorId)
        progressBar.color = color
        timer.foreColor = ApplicationPOMO.data.projects[pos].colorId

        //spark button image and color
        sparkButton.setColors(color, color)
    }

    override fun onStart() {
        super.onStart()
    }

    fun sessionEnded()
    {
        alert.sessionEnded()
        EventBus.getDefault().post(WorkSessionEvent(projectId, sessionLength, true))
    }
}