package com.example.pomotimetracker

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.sip.SipSession
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.support.v7.widget.AppCompatImageButton
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*

class ActivityEditProject : AppCompatActivity() {
    //views
    private var colorPicks = mutableListOf<ImageButton>()
    private lateinit var colorPicker : ImageView
    private lateinit var selectedColor : ImageButton
    private lateinit var constraintLayout: ConstraintLayout
    private lateinit var saveButton : Button
    private lateinit var etProjectName : EditText
    private lateinit var etWeeklyGoal : EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_project)

        var toolbar = findViewById<android.support.v7.widget.Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        //find views
        constraintLayout = findViewById(R.id.parentConstraintLayout)
        colorPicks.add(0, findViewById(R.id.colorPick1))
        colorPicks.add(1, findViewById(R.id.colorPick2))
        colorPicks.add(2, findViewById(R.id.colorPick3))
        colorPicks.add(3, findViewById(R.id.colorPick4))
        colorPicks.add(4, findViewById(R.id.colorPick5))
        colorPicks.add(5, findViewById(R.id.colorPick6))
        colorPicks.add(6, findViewById(R.id.colorPick7))
        colorPicks.add(7, findViewById(R.id.colorPick8))
        colorPicker = findViewById(R.id.image_colorSelector)
        saveButton = findViewById(R.id.btn_save)
        etProjectName = findViewById(R.id.editText_projectName)
        etWeeklyGoal = findViewById(R.id.editText_weeklyGoal)
        selectedColor = colorPicks[0]

        //set focus on project name
        etProjectName.requestFocus()
        //show keyboard
        val inm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)


        var listener = View.OnClickListener{
            //move color picker to the selected color
            selectedColor = findViewById(it.id)
            colorPicker.imageTintList = selectedColor.imageTintList

            val constraintSet = ConstraintSet()
            constraintSet.clone(constraintLayout)
            constraintSet.connect(colorPicker.id, ConstraintSet.RIGHT, it.id, ConstraintSet.RIGHT, 0)
            constraintSet.connect(colorPicker.id, ConstraintSet.LEFT, it.id, ConstraintSet.LEFT, 0)
            constraintSet.connect(colorPicker.id, ConstraintSet.TOP, it.id, ConstraintSet.TOP, 0)
            constraintSet.connect(colorPicker.id, ConstraintSet.BOTTOM, it.id, ConstraintSet.BOTTOM, 0)
            constraintSet.applyTo(constraintLayout)
        }

        //set onclick for color picks
        for (pick in colorPicks){
            pick.setOnClickListener(listener)
        }

        //save button
        saveButton.setOnClickListener{
            //close activity and return data
            val i = Intent()
            Log.w(TAG, etProjectName.text.toString())
            i.putExtra("ProjectName", etProjectName.text.toString())
            i.putExtra("WeeklyGoal", etWeeklyGoal.text.toString())
            i.putExtra("Color", colorPickerToColor(selectedColor.id).toString())
            setResult(Activity.RESULT_OK, i)
            finish()
        }
    }

    //pass in int of selected color picker
    //return color int
    private fun colorPickerToColor(picker : Int): Int{
        return when(picker){
            R.id.colorPick1 -> R.color.colorAccent1
            R.id.colorPick2 -> R.color.colorAccent2
            R.id.colorPick3 -> R.color.colorAccent3
            R.id.colorPick4 -> R.color.colorAccent4
            R.id.colorPick5 -> R.color.colorAccent5
            R.id.colorPick6 -> R.color.colorAccent6
            R.id.colorPick7 -> R.color.colorAccent7
            R.id.colorPick8 -> R.color.colorAccent8
            else -> R.color.colorWhite
        }
    }
}
