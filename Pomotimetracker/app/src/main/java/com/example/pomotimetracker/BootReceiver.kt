package com.example.pomotimetracker

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class BootReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if(Intent.ACTION_BOOT_COMPLETED.equals(intent?.action))
        {
            val i = Intent(context, Activity_projects::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            context?.startActivity(i)
        }
    }
}