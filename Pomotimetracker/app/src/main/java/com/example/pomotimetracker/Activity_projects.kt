package com.example.pomotimetracker

import Utility.AdapterProject
import Utility.Project
import Utility.SwipeToMoveToActivity
import android.app.Activity
import android.app.Application
import android.content.Intent
import android.nfc.Tag
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.CalendarContract
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.widget.TextView

class Activity_projects : AppCompatActivity() {
    val CREATE_PROJECT_REQUEST = 1
    private lateinit var app : ApplicationPOMO

    private lateinit var recyclerView: RecyclerView
    private val viewAdapter = AdapterProject(ApplicationPOMO.data.projects)
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var itemTouchHelper : ItemTouchHelper

    private lateinit var btn_createProject : FloatingActionButton


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_projects)

        app = application as ApplicationPOMO
        //show projects in recylcer view
        viewManager = LinearLayoutManager(this)

        recyclerView = findViewById<RecyclerView>(R.id.recyclerView_projects).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

        itemTouchHelper = ItemTouchHelper(SwipeToMoveToActivity(viewAdapter, this))
        itemTouchHelper.attachToRecyclerView(recyclerView)

        //findviews
        btn_createProject = findViewById(R.id.actionButton_createProject)

        btn_createProject.setOnClickListener{
            //open create project activity
            var i = Intent(this, ActivityEditProject::class.java)
            startActivityForResult(i, 1)
        }

        //TODO: REMOVE THIS (TESTING ONLY)
        //removes all data
        val txt_Projects = findViewById<TextView>(R.id.textView_projects)
        txt_Projects.setOnClickListener{
            ApplicationPOMO.data.projects.clear()
            viewAdapter.notifyDataSetChanged()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == CREATE_PROJECT_REQUEST){
            if(resultCode == Activity.RESULT_OK){
                //create new project and push it to data
                val project = Project()
                project.name = data?.getStringExtra("ProjectName")?: ""
                project.goal = data?.getStringExtra("WeeklyGoal")?.toInt()?: 0
                project.colorId = data?.getStringExtra("Color")?.toInt()?: R.color.colorWhite

                ApplicationPOMO.data.projects.add(project)

                viewAdapter.notifyDataSetChanged()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        app.save()
    }

    fun moveToTimer(projectPosition : Int){
        Log.w(TAG, "timer: " +  ApplicationPOMO.data.projects[projectPosition].name)

        val i = Intent(ApplicationPOMO.context, MainActivity::class.java)
        i.putExtra("Pos", projectPosition.toString())
        startActivity(i)
    }
    fun moveToOverview(projectPosition: Int){
        Log.w(TAG, "overview: " + ApplicationPOMO.data.projects[projectPosition].name)
    }
}
