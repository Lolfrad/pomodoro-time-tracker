package Utility

import android.content.res.ColorStateList
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.pomotimetracker.ApplicationPOMO
import com.example.pomotimetracker.R
import com.example.pomotimetracker.TAG
import kotlinx.android.synthetic.main.rv_projectlayout.view.*
import org.intellij.lang.annotations.JdkConstants

class AdapterProject(private val projects : MutableList<Project>) : RecyclerView.Adapter<AdapterProject.ViewHolder>(){
    class ViewHolder(val v: View) : RecyclerView.ViewHolder(v)
    {
        val txtProjectName = v.findViewById<TextView>(R.id.textView_projectName)
        val txtProjectLevel = v.findViewById<TextView>(R.id.textView_level)
        val iconProjectColor = v.findViewById<ImageView>(R.id.icon_rect)
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterProject.ViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.rv_projectlayout, p0, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.txtProjectName.text = projects[p1].name
        p0.txtProjectLevel.text = projects[p1].level.toString()
        //set color
        val color = ContextCompat.getColor(ApplicationPOMO.context, projects[p1].colorId)
        p0.iconProjectColor.imageTintList = ColorStateList.valueOf(color)
        p0.txtProjectLevel.setTextColor(color)
    }

    override fun getItemCount(): Int {
        return projects.size
    }
}