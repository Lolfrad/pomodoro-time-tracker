package Utility

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.Icon
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.text.style.DynamicDrawableSpan
import android.util.Log
import com.example.pomotimetracker.Activity_projects
import com.example.pomotimetracker.ApplicationPOMO
import com.example.pomotimetracker.R
import com.example.pomotimetracker.TAG

class SwipeToMoveToActivity(private val myAdapter : AdapterProject, private val projectActivity : Activity_projects) : ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.RIGHT or ItemTouchHelper.LEFT) {

    private val iconOverview = ContextCompat.getDrawable(ApplicationPOMO.context, R.drawable.ic_overviewrect) as Drawable
    private val iconTimer = ContextCompat.getDrawable(ApplicationPOMO.context, R.drawable.ic_timericon) as Drawable
    private var icon = iconOverview

    private var background = ColorDrawable(Color.WHITE)
    private var swipeDirection = 0

    override fun onSwiped(p0: RecyclerView.ViewHolder, p1: Int) {
        myAdapter.notifyItemChanged(p0.adapterPosition)
        when(swipeDirection){
            ItemTouchHelper.LEFT -> projectActivity.moveToTimer(p0.adapterPosition)
            ItemTouchHelper.RIGHT -> projectActivity.moveToOverview(p0.adapterPosition)
            else -> Log.e(TAG, "Default swipe direction")
        }
    }
    override fun onMove(p0: RecyclerView, p1: RecyclerView.ViewHolder, p2: RecyclerView.ViewHolder): Boolean {
        return true
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)

        //positions setup
        val backgroundCornerOffset = 25
        val itemView = viewHolder.itemView
        val iconMargin = (itemView.height - iconOverview.intrinsicHeight) / 2
        val iconTop = itemView.top + (itemView.height - iconOverview.intrinsicHeight) / 2
        val iconBottom = iconTop + iconOverview.intrinsicHeight

        //set background and icon positions
        if(dX > 0)
        {
            icon = iconTimer

            val iconLeft = itemView.left + iconMargin
            val iconRight = itemView.left + iconMargin + iconOverview.intrinsicWidth

            background.setBounds(itemView.left, itemView.top, itemView.left + dX.toInt() + backgroundCornerOffset, itemView.bottom)
            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom)

            swipeDirection = ItemTouchHelper.LEFT
        }
        else if(dX < 0)
        {
            icon = iconOverview

            val iconLeft = itemView.right - iconMargin - iconOverview.intrinsicWidth
            val iconRight = itemView.right - iconMargin

            background.setBounds(itemView.right + dX.toInt() - backgroundCornerOffset, itemView.top, itemView.right, itemView.bottom)
            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom)

            swipeDirection = ItemTouchHelper.RIGHT
        }
        else
            background.setBounds(0,0,0,0)

        //draw
        background.draw(c)
        icon.draw(c)
    }
}