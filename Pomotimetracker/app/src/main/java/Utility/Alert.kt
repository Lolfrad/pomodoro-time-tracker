package Utility

import android.os.Build
import android.os.VibrationEffect
import com.example.pomotimetracker.MainActivity

class Alert(mainActivity: MainActivity) {
    private val vibrateRepeats = longArrayOf(0, 650, 300, 650, 300, 650)
    private val vibrateAmplitudes = intArrayOf(0, 255, 0, 255, 0, 255)

    private val vibrationms : Long = 750
    private val activity = mainActivity

    fun sessionEnded()
    {
        if(activity.vibratorService.hasVibrator())
        {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                activity.vibratorService.vibrate(VibrationEffect.createWaveform(vibrateRepeats, vibrateAmplitudes, -1))
            }
            else
            {
                activity.vibratorService.vibrate(vibrateRepeats, -1)
            }
        }
    }
}
