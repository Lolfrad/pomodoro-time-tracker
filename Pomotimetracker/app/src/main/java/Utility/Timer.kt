package Utility

import android.os.Handler
import android.support.v4.content.ContextCompat
import android.util.Log
import com.example.pomotimetracker.MainActivity
import com.example.pomotimetracker.R
import com.example.pomotimetracker.TAG

class Timer(private val mainActivity : MainActivity) {
    private var startTime: Long = 0        //start time of the timer in millis

    private var timerHandler = Handler()
    private lateinit var timerRunnable: Runnable

    //text colors
    var backColor = R.color.colorBackgroundLit
    var foreColor = R.color.colorWhite

    init {
        //timer initialization
        timerRunnable = Runnable()
        {
            var millis: Long = System.currentTimeMillis() - startTime
            //set progress
            var progress: Float = (millis.toFloat() / mainActivity.sessionLength.toFloat()) * 100

            if (progress < 0.5)     //visual bug workaround
                progress = 0.5f

            mainActivity.progressBar.setRadialProgress(progress)


            //timer
            var seconds: Int = (millis / 1000).toInt()
            var minutes: Int = seconds / 60
            seconds %= 60

            if (seconds < 10)
                mainActivity.timerText.text = "${minutes}:0${seconds}"
            else
                mainActivity.timerText.text = "${minutes}:${seconds}"

            if(progress >= 100) //end session if done
                mainActivity.sessionEnded()
            else    //run again in 100 milliseconds
                timerHandler.postDelayed(timerRunnable, 100)
        }
    }

    public fun StartSession()
    {
        //start timer
        startTime = System.currentTimeMillis()

        //set timer text color
        var color = ContextCompat.getColor(mainActivity.applicationContext,
            foreColor
        )
        mainActivity.timerText.setTextColor(color)

        timerHandler.post(timerRunnable)
    }
    public fun PauseSession()
    {
        //set timer text color
        var color = ContextCompat.getColor(mainActivity.applicationContext,
            backColor
        )
        mainActivity.timerText.setTextColor(color)

        //pause timer
        timerHandler.removeCallbacks(timerRunnable)
    }
}
