package Utility

class WorkSessionEvent (private val projectId : Int, private val length : Long, private val finished : Boolean) {
    fun getProjectId(): Int{
        return projectId
    }
    fun getSessionLength() : Long{
        return length
    }
    fun getSessionResult() : Boolean{
        return finished
    }
}