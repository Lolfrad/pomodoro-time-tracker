package Utility

import android.graphics.Color
import android.util.Log
import com.example.pomotimetracker.R
import com.example.pomotimetracker.TAG

class Project {
    var name = ""
    var colorId = R.color.colorWhite
    var goal = 1
    var level = 1
    var experience = 0
    var expNeeded = 3f
    var sessions = 0

    fun calculateNewExpNeeded(){
        expNeeded += 0.5f
    }

    fun levelUp()
    {
        experience = 0
        level ++
        calculateNewExpNeeded()
    }

    fun sessionFinish()
    {
        experience++
        sessions++
        if(experience >= expNeeded)
            levelUp()
    }
}