package Utility

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.util.Log
import android.view.View
import com.example.pomotimetracker.R
import com.example.pomotimetracker.TAG

class RadialProgressBar : View {
    var strokeWidth = 4.0f
    var progress = 0.0f
    var min = 0
    var max = 0

    var startAngle = -90
    var color = R.color.colorWhite
        set(value){
            field = value
            resetPaint()

            invalidate()
        }
    var backColor = Color.DKGRAY

    var rectF = RectF()

    var backgroundPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    var foregroundPaint = Paint(Paint.ANTI_ALIAS_FLAG)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs)
    }

    fun init(context: Context, attrs: AttributeSet){
        rectF = RectF()

        var typedArray = context.theme.obtainStyledAttributes(attrs,
            R.styleable.RadialProgressBar, 0, 0)

        try {
            strokeWidth = typedArray.getFloat(R.styleable.RadialProgressBar_progressBarThickness, strokeWidth)
            progress = typedArray.getFloat(R.styleable.RadialProgressBar_progress, progress)
            color = typedArray.getColor(R.styleable.RadialProgressBar_progressBarColor, color)
            backColor = typedArray.getColor(R.styleable.RadialProgressBar_progressBackColor, color)
            min = typedArray.getInt(R.styleable.RadialProgressBar_min, min)
            max = typedArray.getInt(R.styleable.RadialProgressBar_max, max)
        } finally {
            typedArray.recycle()
        }

        if (backgroundPaint != null) {
            resetPaint()
        }
    }

    private fun resetPaint(){
        backgroundPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        backgroundPaint.color = backColor;
        backgroundPaint.style = Paint.Style.STROKE
        backgroundPaint.strokeWidth = strokeWidth

        foregroundPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        foregroundPaint.color = color
        foregroundPaint.style = Paint.Style.STROKE
        foregroundPaint.strokeWidth = strokeWidth
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val height = getDefaultSize(suggestedMinimumHeight, heightMeasureSpec)
        val width = getDefaultSize(suggestedMinimumWidth, widthMeasureSpec)
        val min = Math.min(width, height)

        setMeasuredDimension(min, min)

        rectF.set(0 + strokeWidth / 2, 0 + strokeWidth / 2, min - strokeWidth / 2, min - strokeWidth / 2)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        if (canvas == null)
            return


        canvas.drawOval(rectF, backgroundPaint)
        val angle: Float = 360 * progress / max

        Log.i(TAG, "min: $min, max: $max, progress: $progress, angle: $angle")

        canvas.drawArc(rectF, startAngle.toFloat(), angle, false, foregroundPaint)
    }

    fun setRadialProgress(progress: Float){
        this.progress = progress
        invalidate()
    }
}
